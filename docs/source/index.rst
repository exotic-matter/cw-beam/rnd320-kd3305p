Welcome to the Polarity Switcher Documentation!
===============================================

.. image:: https://badge.fury.io/py/polarity-switcher.svg
    :target: https://badge.fury.io/py/polarity-switcher
    :alt: PyPI Latest Version

.. image:: https://gitlab.ethz.ch/exotic-matter/cw-beam/polarity-switcher/badges/master/pipeline.svg
    :target: https://gitlab.ethz.ch/exotic-matter/cw-beam/polarity-switcher/-/commits/master
    :alt: Pipeline Status

.. image:: https://gitlab.ethz.ch/exotic-matter/cw-beam/polarity-switcher/badges/master/coverage.svg
    :target: https://gitlab.ethz.ch/exotic-matter/cw-beam/polarity-switcher/-/commits/master
    :alt: Coverage Report

.. image:: https://readthedocs.org/projects/polarity-switcher/badge/?version=latest
    :target: https://polarity-switcher.readthedocs.io/en/latest/?badge=latest
    :alt: Documentation Status

.. image:: https://img.shields.io/badge/License-GPLv3-blue.svg
    :target: https://www.gnu.org/licenses/gpl-3.0
    :alt: License: GPL v3

To be completed...



.. toctree::
    :maxdepth: 1

    Welcome <self>
    Readme <project/README.md>
    api
    Changelog <project/CHANGELOG.md>
    Contributing <project/CONTRIBUTING.md>
    License <project/LICENSE.md>
