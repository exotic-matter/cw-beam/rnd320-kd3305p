""" Daemon TCP server. The server will run indefinitely
listening on the specified TCP (see the
:class:`~lab_utils.socket_comm.Server` documentation).
When a client connects and sends a message string, the
message parser will call the appropriate method. The
following commands are supported by the parser (options
must be used with a double dash \\- \\-):

+-----------+-----------------------+---------------------------------------------------------------------------+
| quit      |                       | Stops the daemon and cleans up database and serial port                   |
+-----------+-----------------------+---------------------------------------------------------------------------+
| status    |                       | TODO: Not implemented yet                                                 |
+-----------+-----------------------+---------------------------------------------------------------------------+
| tpg_256a  | on/off/restart        | Connects / disconnects / restarts the TPG 256A device                     |
+           +-----------------------+---------------------------------------------------------------------------+
|           | test                  | Performs a serial port test and returns the device firmware               |
+           +-----------------------+---------------------------------------------------------------------------+
|           | config {file}         | Reloads the default (or given) config file (logging is stopped)           |
+           +-----------------------+---------------------------------------------------------------------------+
|           | gauge-info            | Returns gauge type, status and latest value                               |
+           +-----------------------+---------------------------------------------------------------------------+
|           | single-readout        | Performs a single read-out to the device (logging is stopped)             |
+-----------+-----------------------+---------------------------------------------------------------------------+
| logging   | start / stop          | Launches or stops the separate device monitoring thread                   |
+           +-----------------------+---------------------------------------------------------------------------+
|           | terminal              | Prints output to the terminal with *info* level                           |
+           +-----------------------+---------------------------------------------------------------------------+
|           | use-database          | Enables data saving to a PostgreSQL database                              |
+-----------+-----------------------+---------------------------------------------------------------------------+

"""

# Imports
from typing import List
from serial import SerialException
import argparse
from psycopg2 import DatabaseError
import configparser
from configparser import Error as ConfigError

# Third party
from lab_utils.socket_comm import Server
from lab_utils.custom_logging import getLogger

# Local
from .RND320_KD3305P import RND320_KD3305P, Channel
from .Monitor import Monitor
from .__project__ import (
    __documentation__ as docs_url,
    __description__ as prog_desc,
    __module_name__ as mod_name,
)


class Daemon(Server):
    """ Base class of the daemon, derived from
    :class:`~lab_utils.socket_comm.Server`. The daemon
    holds pointers to the :attr:`device` driver and the
    :attr:`monitor` thread, and communicates with them
    upon message reception. """

    # Attributes
    max_devices: int = 10           #: Maximum number of devices
    devices: List[RND320_KD3305P]   #: Device handlers.
    monitor: Monitor = None         #: Monitor thread.
    config_file: str                #: The configuration file.

    use_db: bool
    db_file: str
    use_terminal: bool

    def __init__(self,
                 config_file: str = None,
                 pid_file_name: str = None,
                 host: str = None,
                 port: int = None,
                 autostart: bool = False,
                 device_config_file: str = None,
                 database_config_file: str = None,
                 ):
        """ Initializes the :class:`Daemon` object.
        The :attr:`device` constructor is called and
        serial connection is established.

        Parameters
        ----------
        config_file : str, optional
            See parent class :class:`~lab_utils.socket_comm.Server`.

        pid_file_name : str, optional
            See parent class :class:`~lab_utils.socket_comm.Server`.

        host : int, optional
            See parent class :class:`~lab_utils.socket_comm.Server`.

        port : int, optional
            See parent class :class:`~lab_utils.socket_comm.Server`.

        autostart : bool, optional
            Connect to the device and start monitoring.

        device_config_file : str, optional
            Configuration file for the Polarity Switcher :attr:`device`.

        database_config_file : str, optional
            Configuration file for the database. If given and
            :paramref:`~Daemon.__init__.autostart` is 'True',
            a :class:`Monitor` thread will be launched with
            database option active.

        Raises
        ------
        :class:`configparser.Error`
            Configuration file error

        :class:`LockError`
            The PID file could not be locked (see parent
            class :class:`~lab_utils.socket_comm.Server`).

        :class:`OSError`
            Socket errors (see parent class
            :class:`~lab_utils.socket_comm.Server`).

        :class:`~serial.SerialException`
            The connection to the :attr:`device` has failed

        :class:`psycopg2.DatabaseError`
            Database initialization error
        """

        # Initialize attributes
        self.devices = []
        self.use_db = database_config_file is not None
        self.db_file = database_config_file
        self.use_terminal = False

        # Call the parent class initializer, might raise ConfigError, LockError or OSError
        super().__init__(
            config_file=config_file,
            pid_file_name=pid_file_name,
            host=host,
            port=port,
        )

        # Add custom arguments to the message parser
        self.update_parser()

        # Read configuration file, might raise ConfigError
        # Initialize devices, might raise  SerialException
        self.read_config_file(device_config_file)

        # Autostart?
        if not autostart:
            return
        else:
            self.logger.info('Launching auto-start sequence')

        # Start background monitor thread, might raise ConfigError or DatabaseError
        self.monitor = Monitor(
            devices=self.devices,
            name='Daemon Thread',
            terminal_flag=False,  # the autostart option is meant to be used with supervisord, no terminal output
            database_flag=database_config_file is not None,
            database_config_file=database_config_file,
        )
        self.logger.info('Monitor thread launched!')

    def read_config_file(self, new_config_file: str = None):
        """ Loads the RND320 Power Supply configuration from a file.
        If :paramref:`~Daemon.read_config_file.new_config_file`
        is not given, the latest :attr:`config_file` is re-loaded;
        if it is given and the file is successfully parsed,
        :attr:`config_file` is updated to the new value.

        Parameters
        ----------
        new_config_file : str, optional
            New configuration file to be loaded.

        Raises
        ------
        :class:`configparser.Error`
           Configuration file error

        :class:`~serial.SerialException`
            The connection to the device has failed

        :class:`RuntimeError`
           Device busy for too long.
        """

        # Configuration file, if given
        if new_config_file is not None:
            getLogger().info('New configuration file: {}'.format(new_config_file))
            self.config_file = new_config_file

        # Initialize config parser and read file
        getLogger().info("Loading configuration file %s", self.config_file)
        config_parser = configparser.ConfigParser()
        config_parser.read(self.config_file)

        # Load global configuration
        timeout = config_parser.getfloat(section='Connection', option='timeout')
        refresh_rate = config_parser.getint(section='Connection', option='refresh_rate')

        # Check sanity
        if refresh_rate < 1:
            raise ConfigError('Invalid logging refresh rate: {}'.format(refresh_rate))

        # Disconnect previous devices and delete list
        for dev in self.devices:
            if dev.active:
                getLogger().info('Disconnecting device {}'.format(dev.device_info))
                dev.disconnect()
        self.devices = []

        # Look for devices
        list_labels = []
        getLogger().debug('Scanning for devices')
        for i in range(self.max_devices):
            sec_name = 'Device_{}'.format(i+1)
            if config_parser.has_section(sec_name)\
                    and config_parser.getboolean(sec_name, 'active'):
                getLogger().debug('Found active device {}'.format(sec_name))
                default_output: bool = config_parser.get(sec_name, 'output') in ['On', 'ON', 'on']
                address = config_parser.get(sec_name, 'address')
                channels: List[Channel] = []
                for ch in range(2):
                    sub_sec_name = 'Channel_{}.{}'.format(i+1, ch + 1)
                    if config_parser.has_section(sub_sec_name) and config_parser.getboolean(sub_sec_name, 'active'):
                        log_flag = config_parser.getboolean(sub_sec_name, 'logging')
                        v_def = config_parser.getfloat(sub_sec_name, 'voltage')
                        i_def = config_parser.getfloat(sub_sec_name, 'current')
                        label = ''
                        if log_flag:
                            label = config_parser.get(sub_sec_name, 'label')
                            if label == '' or label in list_labels:
                                raise ConfigError('Invalid label {}'.format(label))
                            else:
                                list_labels.append(label)
                        getLogger().debug('Found channel {} - {:12} - {:12}'.format(
                            ch,
                            label,
                            'Logging' if log_flag else 'Not logging'
                        ))
                        channels.append(Channel(
                            id_number=ch,
                            active=True,
                            logging=log_flag,
                            label=label,
                            v_def=v_def,
                            i_def=i_def
                        ))
                    else:
                        # Add channel, inactive by default
                        channels.append(Channel(id_number=ch))

                self.devices.append(RND320_KD3305P(
                    device_id=i,
                    active=True,
                    address=address,
                    timeout=timeout,
                    channels=channels,
                    default_output=default_output
                ))
                getLogger().info('Added device {}'.format(self.devices[-1].device_info))
                for ch in channels:
                    if ch.active:
                        getLogger().info('Channel {} - {} - {}'.format(
                            ch.id + 1,
                            ch.label,
                            'Logging' if ch.logging else 'Not logging'
                        ))
                    else:
                        getLogger().info('Channel {} - Not active'.format(ch.id + 1))
        getLogger().debug('End of configuration process')

    def update_parser(self):
        """ Sets up the message
        :attr:`~lab_utils.socket_comm.Server.parser`. """

        self.logger.debug('Setting up custom message parser')

        # Set some properties of the base class argument parser
        self.parser.prog = mod_name
        self.parser.description = prog_desc
        self.parser.epilog = 'Check out the package documentation for more information:\n{}'.format(docs_url)

        # Subparsers for each acceptable _command
        # 0. STATUS
        sp_status = self.sp.add_parser(
            name='status',
            description='checks the status of the daemon',
        )
        sp_status.set_defaults(
            func=self.status,
            which='status')
        sp_status.add_argument(
            '--update,-u',
            action='store_true',
            help='manually update device status',
            default=False,
            dest='update',
        )

        # 1. RECONFIGURE
        sp_reconfigure = self.sp.add_parser(
            name='reconfigure',
            description='reloads the configuration file',
        )
        sp_reconfigure.set_defaults(
            func=self.reconfigure,
            which='reconfigure')
        sp_reconfigure.add_argument(
            '--filename,-f',
            dest='filename',
            type=str,
            help='new configuration file',
        )

        # 2. RESTART
        sp_restart = self.sp.add_parser(
            name='restart',
            description='restart a RND320 Power Supply device',
        )
        sp_restart.set_defaults(
            func=self.restart,
            which='restart'
        )
        sp_restart.add_argument(
            '--device-id, -dev',
            dest='id',
            type=int,
            choices=range(self.max_devices),
            help='device ID',
            required=True
        )

        # 3. OUTPUT
        sp_output = self.sp.add_parser(
            name='output',
            description='sets an output of an RND320 Power Supply device',
        )
        sp_output.set_defaults(
            func=self.output,
            which='output'
        )
        sp_output.add_argument(
            '--device-id, -dev',
            dest='device',
            type=int,
            choices=range(1, self.max_devices + 1),
            help='device ID',
            required=True
        )
        sp_output.add_argument(
            '--channel-id, -ch',
            dest='channel',
            type=int,
            choices=[1, 2],
            help='channel ID',
            required=True
        )
        sp_output.add_argument(
            '--voltage, -v',
            dest='voltage',
            type=float,
            help='voltage to be set, in Volt',
        )
        sp_output.add_argument(
            '--current, -c',
            dest='current',
            type=float,
            help='current to be set, in Ampere',
        )

        # 4. LOGGING
        sp_logging = self.sp.add_parser(
            name='logging',
            description='manages the logging thread',
        )
        sp_logging.set_defaults(
            func=self.logging,
            which='logging'
        )
        sp_g2 = sp_logging.add_mutually_exclusive_group()
        sp_g2.add_argument(
            '--start',
            action='store_true',
            help='starts the monitor thread',
            default=False,
            dest='start',
        )
        sp_g2.add_argument(
            '--stop',
            action='store_true',
            help='stops the monitor thread',
            default=False,
            dest='stop',
        )
        sp_logging.add_argument(
            '--terminal',
            action='store_true',
            help='prints the monitor output to the application logging sink with INFO level',
            default=False,
            dest='terminal',
        )
        sp_logging.add_argument(
            '--use-database',
            default=argparse.SUPPRESS,  # If --use-database is not given it will not show up in the namespace
            nargs='?',  # If --use-database is given it may be used with or without an extra argument
            const=None,  # If --use-database is given without an extra argument, 'dest' = None
            help='logs data to a PostgreSQL database using the given config file, or the default one',
            dest='database_config_file',
        )

    def quit(self):
        """ Stops the daemon, called with message 'quit'.
        The method overrides the original
        :meth:`~lab_utils.socket_comm.Server.quit` to do
        proper clean-up of the monitoring
        :attr:`thread<monitor>` and the :attr:`device`
        handler.
        """

        self.logger.info('Launching quitting sequence')

        # Monitor
        if self.monitor is not None and self.monitor.is_alive():
            if self.monitor.stop():
                self.reply += 'Monitor thread stopped\n'
            else:
                self.reply += 'Thread error! Monitor thread did not respond to the quit signal and is still running\n'

        # Serial connection
        for device in self.devices:
            if device.active:
                device.disconnect()
        self.reply += 'Clean-up: devices are now off\n'
        self.logger.info('Serial connections closed')

        self.logger.info('Stopping daemon TCP server now')
        self.reply += 'Stopping daemon TCP server now'

    # 0. STATUS
    def status(self):
        """ Return device status.
        """
        # Update device status?
        if self.namespace.update:
            if self._is_logging():
                self.reply += 'Device is logging, no manual status update possible'
            else:
                for dev in self.devices:
                    if dev.active:
                        dev.update_status()

        # Header
        header = '| {:^22} | {:^6} | {:^45} | {:^45} |'.format(
            'Device',
            'Output',
            'Channel 1',
            'Channel 2'
        )
        separator = ''.join('-' for _ in range(len(header)))
        self.reply = separator + '\n' + header + '\n' + separator + '\n'

        # Loop over devices
        for dev in self.devices:
            if dev.active:
                ch_str: List[str] = []
                for ch in dev.channels:
                    if ch.active:
                        ch_str.append('{:>20} | {:5.2f} V | {:5.3f} A | {}'.format(
                            ch.label,
                            ch.v_out,
                            ch.i_out,
                            'CV' if ch.mode else 'CC'
                        ))
                    else:
                        ch_str.append('Inactive')
                self.reply += '| {:22} | {:^6} | {:39} | {:39} |\n'.format(
                    dev.serial_port.replace('/dev/', ''),
                    'ON' if dev.output else 'OFF',
                    ch_str[0],
                    ch_str[1]
                )

        # Trailer
        self.reply += separator + '\n'

    # 1. RECONFIGURE
    def reconfigure(self):
        """ Reloads the daemon configuration.
        """
        self.logger.info('Method \'reconfigure\' called by the message parser')

        try:
            # Stop logging
            logging_active = self._is_logging()
            if logging_active:
                # Save current configuration of the logging thread
                self.logger.info('Saving logging configuration to relaunch after reconfiguration')
                self.namespace.terminal = self.monitor.terminal_flag
                if self.monitor.database_flag:
                    self.namespace.database_config_file = self.monitor.database_config_file

                # Stop the thread, might raise RuntimeError
                self._stop_logging(quiet=True)

            # Reload configuration
            # New configuration file may be provided as an optional argument
            new_config_file = self.namespace.filename
            # Reload config, might raise ConfigError, RuntimeError or SerialException
            self.logger.debug('Loading configuration file')
            self.read_config_file(new_config_file)

            # Apply device output defaults, might raise ValueError, SerialException or RuntimeError
            self.logger.debug('Applying new device defaults')
            for device in self.devices:
                if device.active:
                    device.apply_defaults()

            # Start logging if necessary, might raise
            if logging_active:
                self.logger.debug('Restarting logging')
                self._start_logging()

        except BaseException as e:
            self.reply += 'Error! {}: {}'.format(type(e).__name__, e)
            return

        self.reply += 'Device was reconfigured\n'
        if self.namespace.filename is not None:
            self.reply += 'New configuration file: {}\n'.format(self.config_file)

        self.reply += 'Config routine completed\n'

    # 2. CONTROL
    def restart(self):
        """ Restarts an RND320 Power Supply. If the Monitor was initially
        logging, it is also stopped and relaunched afterwards.
        """
        self.logger.info('Method \'restart\' called by the message parser')

        # Restart sequence
        try:
            # Is device active?
            if not self.devices[self.namespace.id].active:
                raise RuntimeError('Device {} not active'.format(self.namespace.id))

            # Stop logging, might raise RuntimeError
            logging_active = self._is_logging()
            if logging_active:
                # Save current configuration of the logging thread
                self.logger.debug('Saving logging configuration to relaunch')
                self.namespace.terminal = self.monitor.terminal_flag
                if self.monitor.database_flag:
                    self.namespace.database_config_file = self.monitor.database_config_file

                # Stop the thread, might raise RuntimeError
                self._stop_logging(quiet=True)

            # Restart device, might raise SerialException
            self.devices[self.namespace.id].reconnect()

            # Restart logging
            if logging_active:
                self._start_logging()

        except BaseException as e:
            self.reply += 'Error! {}: {}'.format(type(e).__name__, e)
            return
        self.reply += 'Device was restarted\n'

    # 3. OUTPUT
    def output(self):
        """ Set the output of an RND320 Power Supply device.
        """
        self.logger.info('Method \'output\' called by the message parser')

        try:
            # Useful namespace variables
            device_id = self.namespace.device
            channel = self.namespace.channel
            voltage = self.namespace.voltage
            current = self.namespace.current

            # Is device active?
            if not self.devices[device_id - 1].active:
                raise RuntimeError('Device {} not active'.format(device_id))

            # At least one variables given?
            if voltage is None and current is None:
                raise RuntimeError('No voltage or current provided')

            # Set output
            self.logger.debug('Setting device {}, channel {} output to {} V, {} A'.format(
                device_id,
                channel,
                voltage,
                current
            ))
            self.devices[device_id - 1].set_output(
                channel_id=channel,
                voltage=voltage,
                current=current
            )
            self.reply += 'Channel {} ({}, output {}) set to {} V, {} A'.format(
                self.devices[device_id - 1].channels[channel].label,
                self.devices[device_id - 1].serial_port,
                channel,
                voltage,
                current
            )

        except BaseException as e:
            self.reply += 'Error! {}: {}'.format(type(e).__name__, e)

    # 4. LOGGING
    def logging(self):
        """ Manages the :attr:`logging thread<monitor>`.
        Provides functionality to start and stop the thread.
        """
        self.logger.debug('Method \'logging\' called by the message parser')

        # Use database
        self.use_db = "database_config_file" in self.namespace
        if self.use_db:
            self.db_file = self.namespace.database_config_file

        # Use terminal
        self.use_terminal = self.namespace.terminal

        # Start
        if self.namespace.start:
            try:
                self._start_logging()
                self.reply += 'Daemon Thread launched\nYou can check its status with the \'status\' option\n'
            except (SerialException, RuntimeError, DatabaseError, ConfigError) as e:
                self.reply += 'Error launching Daemon Thread! {}: {}'.format(type(e).__name__, e)
                return

        # Stop
        if self.namespace.stop:
            try:
                self._stop_logging()
                self.use_db = False
                self.db_file = ''
                self.use_terminal = False
                self.reply += 'Daemon thread stopped\n'
            except RuntimeError as e:
                self.reply += 'Error stopping Daemon Thread! {}: {}'.format(type(e).__name__, e)
                return

    def _is_logging(self) -> bool:
        """ Checks whether the :attr:`logging thread<monitor>` is running.

        Returns
        -------
        bool
            True if the monitor is running, False otherwise.

        """
        return self.monitor is not None and self.monitor.is_alive()

    def _start_logging(self):
        """ Creates and starts the :attr:`logging thread<monitor>`.

        Raises
        ------
        :class:`configparser.Error`
            Database configuration file error.

        :class:`psycopg2.DatabaseError`
            Database error (connection, access...)

        """
        # Check the monitor is not running yet
        if self._is_logging():
            self.logger.warning('Monitor thread is already running')
            self.reply += 'Monitor thread is already running\n'
            return

        # Launch monitor thread, might raise ConfigError or DatabaseError
        self.logger.info('Launching logging thread')
        self.monitor = Monitor(
            devices=self.devices,
            name='Daemon Thread',
            terminal_flag=self.use_terminal,
            database_flag=self.use_db,
            database_config_file=self.db_file,
        )

    def _stop_logging(self, quiet: bool = False):
        """ Signals a running :attr:`logging thread<monitor>` to stop.

        Parameters
        ----------
        quiet : bool, optional
            If True, no output is produced for the initial running check

        Raises
        -------
        :class:'RuntimeError`
            The monitor thread could not be stopped within 5 seconds.
        """

        # Check monitor is actually running
        if not self._is_logging():
            if not quiet:
                self.logger.info('Monitor thread is not running')
                self.reply += 'Monitor thread is not running\n'
            return

        # Signal the monitor to stop, might raise RuntimeError
        self.logger.info('Stopping logging thread')
        self.monitor.stop()
