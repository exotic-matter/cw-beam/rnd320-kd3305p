""" Driver for the RND320-KD3305P Power Supply. The device is
a...

The :class:`RND320_KD3305P` main class manages the interface
to the device and provides connection and communication
methods through USB.
"""

# Imports
from serial import (
    Serial,
    EIGHTBITS,
    SerialException
)
from typing import List
import configparser
from configparser import Error as ConfigError
from threading import Event
from time import sleep

# Third party
from lab_utils.custom_logging import getLogger
from lab_utils.database import DataType


class Channel:
    """ Wrapper for each of the channels"""

    id: int                 #: Channel ID.
    active: bool            #: Active flag
    logging: bool           #: Logging flag
    label: str              #: Label of the output
    db_labels: List[str]    #: Database column label

    # Data
    mode: bool          #: True for CV, False for CC
    v_out: float        #: Actual output voltage
    i_out: float        #: Actual output current
    v_set: float        #: Set output voltage
    i_set: float        #: Set output current

    # Default configuration
    v_def: float        #: Default output voltage
    i_def: float        #: Default output current

    def __init__(
            self,
            id_number: int,
            active: bool = False,
            logging: bool = False,
            label: str = '',
            v_def: float = 0,
            i_def: float = 0
    ):
        self.id = id_number
        self.active = active
        self.logging = logging
        self.set_labels(label)
        self.v_def = v_def
        self.i_def = i_def

    def v_out_query(self) -> str:
        return 'VOUT{}?'.format(self.id+1)

    def i_out_query(self) -> str:
        return 'IOUT{}?'.format(self.id+1)

    def v_set_query(self) -> str:
        return 'VSET{}?'.format(self.id+1)

    def i_set_query(self, ) -> str:
        return 'ISET{}?'.format(self.id+1)

    def v_set_command(self, voltage: float) -> str:
        return 'VSET{}:{}'.format(self.id+1, voltage)

    def i_set_command(self, current: float) -> str:
        return 'ISET{}:{}'.format(self.id+1, current)

    def set_labels(self, label: str):
        """ Sets the feature label and database labels. """
        self.label = label
        self.db_labels = [
            '{}_v'.format(label.lower()),   # Voltage
            '{}_c'.format(label.lower()),   # Current
            '{}_m'.format(label.lower())    # Mode (CC or CV)
        ]


class RND320_KD3305P(object): # noqa (ignore CamelCase convention)
    """ Driver implementation for the RND-320 KD-3305-P
    Power Supply.
    """

    # Attributes
    active: bool                #: Active device flag.
    device_info: str            #: Device information.
    id: int                     #: Device ID
    channels: List[Channel]     #: Wrappers for the analog inputs

    # Serial communication configuration
    serial: Serial      #: Serial port handler.
    serial_port: str    #: Physical address of the device file.
    timeout: float      #: Time-out for serial connection error.
    busy: Event         #: Flag to avoid concurrent access to the device

    # Device setup
    output: bool                #: Device output status
    default_output: bool        #: Default output status
    max_voltage: float          #: Maximum output voltage
    max_current: float          #: Maximum output current

    def __init__(
            self,
            device_id: int,
            active: bool = False,
            address: str = '/dev/RND320-PowerSupply',
            timeout: float = 2,
            channels: List[Channel] = List,
            default_output: bool = False
    ):
        """ Initializes the :class:`RND320_KD3305P` object.

        Parameters
        ----------
        active : bool
            Device is active.

        address : str
            Device physical address.

        timeout : float
            Time-out for serial connection error.

        channels : List[Channel]
            Array of channel information.

        default_output : bool, optional
            Default output status, default is Off (False).

        Raises
        ------
        :class:`~serial.SerialException`
            The connection to the device has failed

        :class:`RuntimeError`
           Device busy for too long.
        """
        # Initialize attributes
        self.id = device_id
        self.active = active
        if not self.active:
            return
        self.serial_port = address
        self.timeout = timeout
        self.channels = channels
        self.busy = Event()
        self.busy.clear()
        self.default_output = default_output
        self.max_voltage = 35.0
        self.max_current = 4.0

        # Connect to the device, might raise SerialException
        self.connect()

        # Read device information
        self.get_info()

        # Apply default outputs, might raise ValueError, SerialException or RuntimeError
        self.apply_defaults()

        # Get device status
        self.update_status()

    def connect(self):
        """ Connects to the RND Power Supply device.

        Raises
        ------
        :class:`~serial.SerialException`
            The connection to the device has failed.
        """
        getLogger().debug('Connecting to RND 320 Power Supply on port %s', self.serial_port)

        # Open serial connection, might raise SerialException
        self.serial = Serial(
            port=self.serial_port,
            timeout=self.timeout,
            write_timeout=self.timeout,
            baudrate=9600,
            bytesize=EIGHTBITS,
            parity='N',
            stopbits=1,
            xonxoff=False,
            rtscts=True,
            dsrdtr=True
        )

        getLogger().info('Connection to RND 320 Power Supply on port %s successful', self.serial_port)
        self._flush()

    def disconnect(self):
        """ Closes the connection to the RND Power Supply.
        """

        getLogger().debug('Closing connection to device {}'.format(self.device_info))
        self.serial.close()
        self.active = False
        getLogger().info('Connection to device {} closed'.format(self.device_info))

    def reconnect(self):
        """ Closes the RND 320 Power Supply device and connects again.

        Raises
        ------
        :class:`~serial.SerialException`
            The connection to the device has failed.
        """
        getLogger().debug('Reconnecting to RND 320 Power Supply on port %s', self.serial_port)

        # Close connection
        self.serial.close()

        # Connect again, might raise SerialException
        self.serial = Serial(
            port=self.serial_port,
            timeout=self.timeout,
            write_timeout=self.timeout,
            baudrate=9600,
            bytesize=EIGHTBITS,
            parity='N',
            stopbits=1,
            xonxoff=False,
            rtscts=True,
            dsrdtr=True
        )
        getLogger().debug('RND 320 Power Supply reconnected on port on port %s', self.serial_port)

    def _query(
            self,
            msg: str,
            n_lines: int = 3,
            decode_hex: bool = False
    ) -> str:
        """ Send the _query string
        :paramref:`~RND320_KD3305P._command.msg` through
        the serial connection. If the port is busy, the
        method waits up to 5 seconds to send the _command.

        Parameters
        ----------
        msg : str
            Query to be sent to the device.

        n_lines : int, optional
            Number of times the readline() method must be used, default is 1.

        decode_hex : bool, optional
            Decode the device response as hexadecimal instead of UTF-8,
            default is False.

        Returns
        -------
        str:
            The reply of the device

        Raises
        ------
        :class:`serial.SerialException`
           Serial communication error

        :class:`RuntimeError`
           Device was busy for too long
        """
        # Avoid concurrent access with the busy flag
        # Try to set the flag for 5 seconds, raise RuntimeError otherwise
        for _ in range(50):
            if not self.busy.is_set():
                self.busy.set()
                getLogger().debug('Device is now locked, ready to receive query \'{}\''.format(msg))
                break
            sleep(0.1)
            getLogger().debug('Device busy, waiting to send query \'{}\'...'.format(msg))
        else:
            raise RuntimeError('Device busy for too long, query \'{}\' not sent'.format(msg))

        # Send the message, might raise SerialException
        getLogger().debug('Sending query \'{}\''.format(msg))
        self.serial.write('{}\n'.format(msg).encode('utf-8'))

        # Read reply, might raise SerialException
        # Sometimes there are empty lines
        for _ in range(n_lines):
            try:
                r = self.serial.readline()
                getLogger().debug('Received reply: \'{}\''.format(r))
                r = r.replace(b'\n', b'').replace(b'\r', b'')
                if decode_hex:
                    r = r.hex()
                else:
                    r = r.decode("utf-8")
                getLogger().debug('Parsed reply: \'{}\''.format(r))
                if len(r) > 1:
                    break
            except UnicodeDecodeError as e:
                getLogger().debug("{}: {}".format(type(e).__name__, e))
        else:
            getLogger().debug('Unlocking device')
            self.busy.clear()
            raise SerialException('Empty response from query {}'.format(msg))

        # Clear the busy flag
        getLogger().debug('Unlocking device')
        self.busy.clear()

        return r

    def _command(self, msg: str):
        """ Send the _command string
        :paramref:`~RND320_KD3305P._command.msg` through
        the serial connection. If the port is busy, the
        method waits up to 5 seconds to send the _command.

        Parameters
        ----------
        msg : str
            Command to be sent to the device.

        Raises
        ------
        :class:`serial.SerialException`
           Serial communication error

        :class:`RuntimeError`
           Device was busy for too long
        """
        # Avoid concurrent access with the busy flag
        # Try to set the flag for 5 seconds, raise RuntimeError otherwise
        for _ in range(50):
            if not self.busy.is_set():
                self.busy.set()
                getLogger().debug('Device is now locked, ready to receive command \'{}\''.format(msg))
                break
            sleep(0.1)
            getLogger().debug('Device busy, waiting to send command \'{}\'...'.format(msg))
        else:
            raise RuntimeError('Device busy for too long, command \'{}\' not sent'.format(msg))

        # Send the message, might raise SerialException
        getLogger().debug('Sending command \'{}\''.format(msg))
        self.serial.write('{}\n'.format(msg).encode('utf-8'))

        # Clear the busy flag
        getLogger().debug('Unlocking device')
        self.busy.clear()

    def _flush(self):
        """ Cleans the input buffer.

        Raises
        ------
        :class:`serial.SerialException`
            General serial connection error.
        """
        getLogger().info('Cleaning input buffer of {}'.format(self.serial_port))
        self.serial.reset_input_buffer()
        getLogger().debug('Device says: {}'.format(
            self.serial.readline().decode("utf-8").replace('\n', ' ').replace('\r', ''))
        )

    def get_info(self):
        """ Reads the identification information from the
         device: manufacturer, model name, software version.

        Raises
        ------
        :class:`serial.SerialException`
            General serial connection error.

        :class:`RuntimeError`
           Device was busy for too long
        """
        getLogger().info('Reading identification information from RND-320 KD-3305P Power Supply')
        self.device_info = self._query('*IDN?', n_lines=3)
        getLogger().info(self.device_info)

    def apply_defaults(self):
        """ Applies the default device values.

        Raises
        ------
        :class:`ValueError`
           Invalid default parameters.

        :class:`SerialException`
            Serial communication error

        :class:`RuntimeError`
           Device busy for too long.
        """
        getLogger().info('Applying default settings to device {}'.format(self.device_info))

        # Apply default output status, might raise SerialException or RuntimeError
        if self.default_output:
            getLogger().info('Switching output ON')
            self._command('OUT1')
        else:
            getLogger().info('Switching output OFF')
            self._command('OUT0')

        # Apply default output of channels
        for ch in self.channels:
            if ch.active:
                getLogger().info('Setting channel {} - {} to {} V , {} A'.format(
                    ch.id + 1,
                    ch.label,
                    ch.v_def,
                    ch.i_def
                ))
                self._command(ch.v_set_command(ch.v_def))
                self._command(ch.i_set_command(ch.i_def))
                ch.v_set = ch.v_def
                ch.i_set = ch.i_def

    def update_status(self):
        """ Reads the status of the device and the active channels.
        """
        getLogger().debug('Reading status of device {}'.format(self.device_info))

        # Get status
        try:
            status = int(self._query('STATUS?', decode_hex=True), 16)
            bits = [True if status & (1 << (7-n)) else False for n in range(7, -1, -1)]
            getLogger().debug('Status of device {}: {} - {}'.format(
                self.device_info,
                status,
                bits
            ))
            self.output = bits[6]
            self.channels[0].mode = bits[0]
            self.channels[1].mode = bits[1]
        except BaseException as e:
            getLogger().error("{}: {}".format(type(e).__name__, e))

        # Read output status
        for ch in self.channels:
            if ch.active:
                try:
                    getLogger().debug('Reading channel {}: {}'.format(ch.id, ch.label))
                    ch.v_out = float(self._query(ch.v_out_query()))
                    ch.i_out = float(self._query(ch.i_out_query()))
                    getLogger().debug('Channel {}: {}V - {}A'.format(ch.id, ch.v_out, ch.i_out))
                except BaseException as e:
                    getLogger().error("{}: {}".format(type(e).__name__, e))
                    ch.v_out = float('NaN')
                    ch.i_out = float('NaN')

    def set_output(
            self,
            channel_id: int,
            voltage: float = None,
            current: float = None
    ):
        """ Sets the voltage and/or current output of channel
        :paramref:`~RND320_KD3305P.set_output.channel_id` to
        :paramref:`~RND320_KD3305P.set_output.voltage` Volt and
        :paramref:`~RND320_KD3305P.set_output.current` Ampere.

        Parameters
        ----------
        channel_id : int
            ID of the channel to set (1 or 2).

        voltage: float, optional
            Voltage to be set, in Volt.

        current: float, optional
            Current to be set, in Ampere.

        Raises
        ------
        :class:`ValueError`
           Invalid parameters.

        :class:`SerialException`
            Serial communication error

        :class:`RuntimeError`
           Device busy for too long.
        """
        # Check channel ID is valid
        if channel_id not in [1, 2]:
            raise ValueError('Invalid channel ID {}'.format(channel_id))

        # Check channel is enabled
        if not self.channels[channel_id-1].active:
            raise ValueError('Channel {} not active'.format(channel_id))

        # Voltage output
        if voltage is not None:
            # Check voltage value appropriateness
            if not self._is_valid_voltage(voltage):
                raise ValueError('Invalid voltage {}'.format(voltage))

            # Set the voltage might raise SerialException or RuntimeError
            self._command(self.channels[channel_id - 1].v_set_command(voltage))

            # No error, update value
            self.channels[channel_id - 1].v_set = voltage
            getLogger().info('Channel {} ({}) set to {} V'.format(
                channel_id,
                self.channels[channel_id - 1].label,
                voltage,
            ))

        # Current output
        if current is not None:
            # Check current value appropriateness
            if not self._is_valid_current(current):
                raise ValueError('Invalid current {}'.format(current))

            # Set the current might raise SerialException or RuntimeError
            self._command(self.channels[channel_id - 1].i_set_command(current))

            # No error, update value
            self.channels[channel_id - 1].i_set = current
            getLogger().info('Channel {} ({}) set to {} A'.format(
                channel_id,
                self.channels[channel_id - 1].label,
                current,
            ))

    def _is_valid_voltage(self, voltage: float) -> bool:
        """ Check whether the given
        :paramref:`~RND320_KD3305P._is_valid_voltage.voltage`
        is a valid value.

        Parameters
        ----------
        voltage : float
            The voltage to be checked.

        Returns
        -------
        bool:
            True if the :paramref:`~RND320_KD3305P._is_valid_voltage.voltage` is valid.
        """
        return 0 <= voltage <= self.max_voltage

    def _is_valid_current(self, current: float) -> bool:
        """ Check whether the given
        :paramref:`~RND320_KD3305P._is_valid_current.current`
        is a valid value.

        Parameters
        ----------
        current : float
            The current to be checked.

        Returns
        -------
        bool:
            True if the :paramref:`~RND320_KD3305P._is_valid_current.current` is valid.
        """
        return 0 <= current <= self.max_current

